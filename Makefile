
CXX=g++

CXXFLAGS=-O3 -g -Wall -fopenmp -std=c++17
LDFLAGS =-g -fopenmp
LIBS    =
INC     =

include make.inc


OBJS=main.o load_matrix.o solver.o util.o
HEADERS=load_matrix.hpp solver.hpp util.hpp


.PHONY: clean

tester: $(OBJS)
	$(CXX) $(LDFLAGS) $(LIBS) $^ -o $@

%.o: %.cpp $(HEADERS)
	$(CXX) $(CXXFLAGS) $(INC) -c $< -o $@

clean:
	rm -f *.o tester

