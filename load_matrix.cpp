
#include "load_matrix.hpp"

#include "util.hpp"


#include <chrono>
#include <fstream>
#include <iostream>
#include <lapack.hh>
#include <vector>

template<class T>
void copy_into_slate(T* data, slate::Matrix<T> A) {
    const int64_t m = A.m();

    int64_t j_offset = 0;
    for (int64_t j = 0; j < A.nt(); ++j) {
        const int64_t nb = A.tileNb(j);
        int64_t i_offset = 0;
        for (int64_t i = 0; i < A.mt(); ++i) {
            const int64_t mb = A.tileMb(i);
            #pragma omp task
            if (A.tileIsLocal(i, j)) {
                A.tileGetForWriting(i, j, slate::LayoutConvert::ColMajor);
                lapack::lacpy(lapack::MatrixType::General, mb, nb,
                              data + i_offset + j_offset*m, m,
                              A(i, j).data(), A(i, j).stride());
            }
            i_offset += mb;
        }
        j_offset += nb;
    }
}



void read_lsms_file(std::string case_path, slate::Matrix<double> A) {
    throw std::invalid_argument( "LSMS reader loads complex valued matrices" );
}
void read_lsms_file(std::string case_path, slate::Matrix<std::complex<double>> A) {

    // cache last loaded file for performance purposes
    static std::vector<std::complex<double>> A_temp;
    static std::string cached_path = "";

    const int64_t m = A.m(), n = A.n();

    if (cached_path != case_path) {
        cached_path = case_path;
        A_temp.resize(m*n);
        std::fstream case_file;
        case_file.open(case_path, std::ios::in);
        if (!case_file.is_open()) {
            throw std::invalid_argument( "Could not open file '"+case_path+"'" );
        }

        int64_t last_i=-1, last_j=-1;
        std::string line;
        while (getline(case_file, line)) {
            if (line[0] == '#') {
                continue;
            }

            int64_t jj = -1, ii = -1;
            double real, imag;
            try {
                size_t line_offset = 0;
                ii   = std::stol(line, &line_offset);
                line = line.substr(line_offset);
                jj   = std::stol(line, &line_offset);
                line = line.substr(line_offset);
                real = std::stod(line, &line_offset);
                line = line.substr(line_offset);
                imag = std::stod(line, &line_offset);
            } catch (const std::invalid_argument& e) {
                std::cerr << "could not read " << ii << "," << jj << " (entry after " << last_i << "," << last_j << ")" << std::endl;
                throw;
            }

            A_temp[ii + jj*m] = std::complex<double>(real, imag);

            last_i = ii; last_j = jj;
        }
    }

    copy_into_slate(A_temp.data(), A);
}


template<class T>
void read_binary_file(std::string case_path, slate::Matrix<T> A) {
    // cache last loaded file for performance purposes
    static std::vector<T> A_temp;
    static std::string cached_path = "";

    const int64_t m = A.m(), n = A.n();

    if (cached_path != case_path) {
        cached_path = case_path;
        A_temp.resize(m*n);

        int mpi_rank = A.mpiRank(), mpi_size;
        MPI_Comm_size(A.mpiComm(), &mpi_size);
        int64_t entries = m*n;
        int64_t local_entries = (entries+mpi_size-1)/mpi_size;
        int64_t local_offset = mpi_rank*local_entries;
        auto entry_size = sizeof(T);

        if (mpi_rank+1 == mpi_size) {
          local_entries = entries - local_entries*(mpi_size-1);
        }

        std::ifstream file(case_path, std::ios::binary);
        file.seekg(local_offset*entry_size);
        file.read((char*)(A_temp.data()+local_offset), local_entries*entry_size);

        MPI_Allreduce(MPI_IN_PLACE, A_temp.data(), entries, mpi_type<T>::value,
                      MPI_SUM, A.mpiComm());
    }

    copy_into_slate(A_temp.data(), A);
}


template<class T>
void matrix_generator(std::string generator, slate::Matrix<T> A) {
    constexpr T zero = 0.0, one = 1.0;
    const int64_t mt = A.mt(), nt = A.nt();
    if (generator == "identity") {
        slate::set(zero, one, A);
    } else if (generator == "rand") {
        #pragma omp parallel for collapse(2)
        for (int64_t j = 0; j < nt; ++j) {
            for (int64_t i = 0; i < mt; ++i) {
                if (A.tileIsLocal(i, j)) {
                    A.tileGetForWriting( i, j, slate::LayoutConvert::ColMajor );
                    auto Aij = A(i, j);
                    T* data = Aij.data();
                    int64_t lda = Aij.stride();
                    // Added local seed array for each process to prevent race condition contention of iseed
                    int64_t tile_iseed[4];
                    tile_iseed[0] = (i/4096) % 4096;
                    tile_iseed[1] = (j/2048) % 4096;
                    tile_iseed[2] = (i)      % 4096;
                    tile_iseed[3] = (j*2)    % 4096;
                    for (int64_t k = 0; k < Aij.nb(); ++k) {
                        lapack::larnv(1, tile_iseed, Aij.mb(), &data[k*lda]);
                    }
                }
            }
        }
    } else if (generator == "randn") {
        #pragma omp parallel for collapse(2)
        for (int64_t j = 0; j < nt; ++j) {
            for (int64_t i = 0; i < mt; ++i) {
                if (A.tileIsLocal(i, j)) {
                    A.tileGetForWriting( i, j, slate::LayoutConvert::ColMajor );
                    auto Aij = A(i, j);
                    T* data = Aij.data();
                    int64_t lda = Aij.stride();
                    // Added local seed array for each process to prevent race condition contention of iseed
                    int64_t tile_iseed[4];
                    tile_iseed[0] = (i/4096) % 4096;
                    tile_iseed[1] = (j/2048) % 4096;
                    tile_iseed[2] = (i)      % 4096;
                    tile_iseed[3] = (j*2+1)  % 4096;
                    for (int64_t k = 0; k < Aij.nb(); ++k) {
                        lapack::larnv(3, tile_iseed, Aij.mb(), &data[k*lda]);
                    }
                }
            }
        }
    } else {
        throw std::invalid_argument( "Unknown matrix generator '"+generator+"'" );
    }
}

bool is_complex_case(std::string reader, std::string case_file) {
    if (reader == "lsms") {
        return true;
    } else if (reader == "binary-z") {
        return true;
    } else if (reader == "binary-d") {
        return true;
    } else if (reader == "generator") {
        return false;
    } else {
        throw std::invalid_argument( "Unknown matrix reader '"+reader+"'" );
    }
}

template<class T>
void load_matrix(std::string reader, std::string case_file, slate::Matrix<T> A) {
    auto timer_start = std::chrono::steady_clock::now();
    if (reader == "lsms") {
        read_lsms_file(case_file, A);
    } else if (reader == "binary-z" || reader == "binary-d") {
        read_binary_file(case_file, A);
    } else if (reader == "generator") {
        matrix_generator(case_file, A);
    } else {
        throw std::invalid_argument( "Unknown matrix reader '"+reader+"'" );
    }
    auto timer_stop = std::chrono::steady_clock::now();
    double time = std::chrono::duration<double>(timer_stop-timer_start).count();
    if (A.mpiRank() == 0) {
        std::cout << "Loaded matrix in " << time << "s" << std::endl;
    }
}



template
void load_matrix<double>(std::string reader, std::string case_file, slate::Matrix<double> A);
template
void load_matrix<std::complex<double>>(std::string reader, std::string case_file, slate::Matrix<std::complex<double>> A);
