
#include "util.hpp"

#include <sstream>

std::vector<std::string> split(const std::string& s, const char c) {
    std::vector<std::string> out;
    std::string entry;
    std::istringstream ss(s);
    while (std::getline(ss, entry, c)) {
        out.push_back(entry);
    }
    return out;
}


MPI_Datatype mpi_type<float >::value = MPI_FLOAT;
MPI_Datatype mpi_type<double>::value = MPI_DOUBLE;
MPI_Datatype mpi_type< std::complex<float>  >::value = MPI_C_COMPLEX;
MPI_Datatype mpi_type< std::complex<double> >::value = MPI_C_DOUBLE_COMPLEX;

