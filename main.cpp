
#include "load_matrix.hpp"
#include "solver.hpp"

#include <iostream>
#include <vector>

#include <blas.hh>

template<class T>
void run_tests(std::string reader, std::string case_file, int64_t n,
               std::vector<Solver> solvers) {

    int nrhs = 1;
    std::string rhs = "randn";
    std::cout << "rhs: " << n << "x" << nrhs << " " << rhs << std::endl;
    for (auto solver : solvers) {
        auto A = solver.alloc_matrix<T>(n, n);
        auto B = solver.alloc_matrix<T>(n, nrhs);

        load_matrix(reader, case_file, A);
        load_matrix("generator", rhs, B);

        solver.run_test(A, B);
    }
}

int main(int argc, char** argv) {
    // initialize libraries
    int provided = 0;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided != MPI_THREAD_MULTIPLE) {
        throw std::invalid_argument("MPI doesn't support thread multiple"); 
    }
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // parse arguments
    if (argc < 1+3) {
        std::cerr << "Too few arguments" << std::endl;
        return -1;
    }

    auto reader = argv[1];
    auto case_file = argv[2];
    auto n = std::stoi(argv[3]);
    
    std::vector<Solver> solvers;

    for (int i = 4; i < argc; ++ i) {
        solvers.push_back(Solver(argv[i]));
    }

    if (is_complex_case(reader, case_file)) {
        run_tests<std::complex<double>>(reader, case_file, n, solvers);
    } else {
        run_tests<double>(reader, case_file, n, solvers);
    }


    MPI_Finalize();
}
