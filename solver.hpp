
#ifndef SOLVER_HPP
#define SOLVER_HPP

#include <slate/slate.hh>

#include <unordered_map>
#include <string>


class Solver {
public:

    std::string name;
    std::unordered_map<std::string, std::string> args;

    Solver(std::string desc);


    template<class T>
    slate::Matrix<T> alloc_matrix(int64_t m, int64_t n);

    template<class T>
    void run_test(slate::Matrix<T> A, slate::Matrix<T> B);

};

#endif // SOLVER_HPP
