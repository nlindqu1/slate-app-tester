

#include "solver.hpp"

#include <chrono>
#include <limits>
#include <stdexcept>

static void parser_skip_whitespace(const std::string& desc, size_t& i) {
    while (i < desc.length() && desc[i] == ' ') {
        i++;
    }
}
static std::string parser_read_token(const std::string& desc, size_t& i) {
    parser_skip_whitespace(desc, i);
    size_t start = i;
    while (i < desc.length()
           && desc[i] != ' ' && desc[i] != ')' && desc[i] != '=' && desc[i] != ',') {
        i++;
    }
    return desc.substr(start, i-start);
}

static char parser_read_character(const std::string& desc, size_t& i, std::string chars) {
    parser_skip_whitespace(desc, i);
    auto idx = chars.find(desc[i]);
    if (idx == std::string::npos) {
        throw std::invalid_argument("Malformed expression"); 
    }
    i++; // move past character
    return chars[idx];
}


static std::string args_get(const std::unordered_map<std::string, std::string> args,
                            const std::string key,
                            const std::string default_val="") {
    auto it = args.find(key);
    if  (it != args.end()) {
        return it->second;
    }
    return default_val;
}



Solver::Solver(std::string desc) : args() {
    size_t i = 0;
    while (i < desc.length() && desc[i] != '(') {
        i++;
    }
    name = desc.substr(0, i);


    if (i < desc.length()) {
        if (desc.back() != ')') {
            throw std::invalid_argument("Malformed expression"); 
        }

        i++; // move past (

        while (true) { // gaurenteed to have a close brace before the end
            std::string key = parser_read_token(desc, i);
            parser_read_character(desc, i, "=");
            std::string val = parser_read_token(desc, i);
            char seperator = parser_read_character(desc, i, ",)");

            args[key] = val;

            if (seperator == ')') {
                break;
            }
        }
    }
}



template<class T>
slate::Matrix<T> Solver::alloc_matrix(int64_t m, int64_t n) {

    auto nb = std::stoi(args_get(args, "nb", "256"));
    auto p  = std::stoi(args_get(args, "p",  "1"));
    auto q  = std::stoi(args_get(args, "q",  "1"));
    auto target = (args_get(args, "target", "host") == "host") ? slate::Target::Host : slate::Target::Devices;

    slate::Matrix<T> A(m, n, nb, p, q, MPI_COMM_WORLD);
    A.insertLocalTiles(target);

    return A;
}

template
slate::Matrix<double> Solver::alloc_matrix(int64_t m, int64_t n);
template
slate::Matrix<std::complex<double>> Solver::alloc_matrix(int64_t m, int64_t n);


template<class T>
void Solver::run_test(slate::Matrix<T> A, slate::Matrix<T> B) {
    using real_t = blas::real_type<T>;

    T one = 1.0;

    auto target = (args_get(args, "target", "host") == "host") ? slate::Target::Host : slate::Target::Devices;
    slate::Options const opts =  {
        {slate::Option::Lookahead,       std::stoi(args_get(args, "la", "1"))},
        {slate::Option::Target,          target},
        {slate::Option::MaxPanelThreads, std::stoi(args_get(args, "mpft", "1"))},
        {slate::Option::InnerBlocking,   std::stoi(args_get(args, "ib", "4"))}
    };

    auto Aref = alloc_matrix<T>(A.m(), A.n());
    slate::copy(A, Aref);
    auto Bref = alloc_matrix<T>(B.m(), B.n());
    slate::copy(B, Bref);

    MPI_Barrier(A.mpiComm());
    auto timer_start = std::chrono::steady_clock::now();
    if (name == "gesv") {
        slate::lu_solve(A, B, opts);
    } else if (name == "gesv_nopiv") {
        slate::lu_solve_nopiv(A, B, opts);
    } else if (name == "gesv_mixed") {
        auto X = B.emptyLike();
        X.insertLocalTiles(target);
        slate::Pivots piv;
        int iter;
        slate::gesvMixed(A, piv, B, X, iter, opts);
        slate::copy(X, B, opts);
        if(A.mpiRank() == 0) std::cout << iter << " iterations\n";
        if (iter < 0 && A.mpiRank() == 0) {
            std::cout << "WARNING: mxpr IR failed after " << (-iter) << " iterations" << std::endl;
        }
    } else if (name == "gesv_mixed_nopiv") {
        auto X = B.emptyLike();
        X.insertLocalTiles(target);
        int iter;
        slate::gesvMixed_nopiv(A, B, X, iter, opts);
        slate::copy(X, B, opts);
        if(A.mpiRank() == 0) std::cout << iter << " iterations\n";
        if (iter < 0 && A.mpiRank() == 0) {
            std::cout << "WARNING: mxpr IR failed after " << (-iter) << " iterations" << std::endl;
        }
    } else if (name == "gels") {
        slate::least_squares_solve(A, B, opts);
    } else if (name == "jacobi") {
        auto X = B.emptyLike();
        X.insertLocalTiles(target);
        slate::copy(B, X, opts);
        auto R = B.emptyLike();
        R.insertLocalTiles(target);

        real_t A_norm = slate::norm(slate::Norm::One, A, opts);
        real_t b_norm = slate::norm(slate::Norm::One, B, opts);
        real_t cte = 100*std::numeric_limits<real_t>::epsilon();
        real_t r_norm, x_norm;

        int64_t i = 0;
        for (; i < 100; i++) {

            slate::copy(B, R, opts);
            slate::gemm(-one, A, X,
                        one, R,
                        opts);
            r_norm = slate::norm(slate::Norm::One, R, opts);
            x_norm = slate::norm(slate::Norm::One, X, opts);
            slate::add(one, R, one, X, opts);
            //if (A.mpiRank()==0) std::cout << i << ": " << r_norm/(A_norm*x_norm+b_norm) << "\n";
            if (r_norm <= cte*(A_norm*x_norm + b_norm)) break;
        }
        if(A.mpiRank() == 0) std::cout << i << " iterations\n";
        slate::copy(X, B, opts);

    } else if (name == "gauss-seidel") {
        auto X = B.emptyLike();
        X.insertLocalTiles(target);
        slate::copy(B, X, opts);
        auto R = B.emptyLike();
        R.insertLocalTiles(target);

        auto L = slate::TriangularMatrix<T>(slate::Uplo::Lower, slate::Diag::NonUnit, A);

        real_t A_norm = slate::norm(slate::Norm::One, A, opts);
        real_t b_norm = slate::norm(slate::Norm::One, B, opts);
        real_t cte = 10*std::numeric_limits<real_t>::epsilon();
        real_t r_norm, x_norm;

        int64_t i = 0;
        for (; i < 100; i++) {

            slate::copy(B, R, opts);
            slate::gemm(-one, A, X,
                        one, R,
                        opts);
            r_norm = slate::norm(slate::Norm::One, R, opts);
            x_norm = slate::norm(slate::Norm::One, X, opts);
            //if (A.mpiRank()==0) std::cout << i << ": " << r_norm/(A_norm*x_norm+b_norm) << "\n";
            if (r_norm <= cte*(A_norm*x_norm + b_norm)) break;
            slate::trsm(slate::Side::Left, one, L, R, opts);
            slate::add(one, R, one, X, opts);
        }
        if(A.mpiRank() == 0) std::cout << i << " iterations\n";
        slate::copy(X, B, opts);


    } else {
        throw std::invalid_argument("Unknown solver '" + name + "'"); 
    }
    auto timer_stop = std::chrono::steady_clock::now();
    double runtime = std::chrono::duration<double>(timer_stop-timer_start).count();

    double B_norm = slate::norm(slate::Norm::One, Bref);
    double X_norm = slate::norm(slate::Norm::One, B);
    double A_norm = slate::norm(slate::Norm::One, Aref);

    slate::multiply(-one, Aref, B, one, Bref);
    double R_norm = slate::norm(slate::Norm::One, Bref);
    double residual = R_norm / (A_norm*X_norm + B_norm);

    if (A.mpiRank() == 0) {
        std::cout << name << "  " << residual << "  " << runtime << std::endl;
    }

}

template
void Solver::run_test<double>(slate::Matrix<double> A, slate::Matrix<double> B);
template
void Solver::run_test<std::complex<double>>(slate::Matrix<std::complex<double>> A, slate::Matrix<std::complex<double>> B);


