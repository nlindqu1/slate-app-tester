
#ifndef LOAD_MATRIX_HPP
#define LOAD_MATRIX_HPP

#include <slate/slate.hh>

#include <string>

bool is_complex_case(std::string reader, std::string case_file);

template<class T>
void load_matrix(std::string reader, std::string case_file, slate::Matrix<T> A);

#endif // LOAD_MATRIX_HPP
