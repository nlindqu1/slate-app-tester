
#ifndef UTIL_HPP
#define UTIL_HPP

#include <complex>
#include <mpi.h>
#include <string>
#include <vector>

std::vector<std::string> split(const std::string& s, const char c = ',');


template <typename scalar_t>
class mpi_type {};

template<>
class mpi_type<float> {
public:
    static MPI_Datatype value; // = MPI_FLOAT
};

template<>
class mpi_type<double> {
public:
    static MPI_Datatype value; // = MPI_DOUBLE
};

template<>
class mpi_type< std::complex<float> > {
public:
    static MPI_Datatype value; // = MPI_C_COMPLEX
};

template<>
class mpi_type< std::complex<double> > {
public:
    static MPI_Datatype value; // = MPI_C_DOUBLE_COMPLEX
};

#endif // UTIL_HPP
